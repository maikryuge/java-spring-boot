package com.ryuge.demo.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import com.ryuge.demo.entities.User;
import com.ryuge.demo.repositories.UserRepository;

import jakarta.persistence.EntityNotFoundException;

public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    @Autowired
    UserService userService;

    @Test
    void testDelete() {
        // Create a user object to be deleted
        User userToDelete = new User();
        userToDelete.setId(1L);
        userToDelete.setName("John Doe");
        userToDelete.setEmail("johndoe@example.com");
        userToDelete.setPhone("1234567890");
        userToDelete.setPassword("password123");

        // Mock the UserService
        UserService userService = Mockito.mock(UserService.class);

        // Mock the behavior of the delete method to return void (since it doesn't
        // return anything)
        doNothing().when(userService).delete(any(Long.class));

        // Call the delete method with the user's ID
        userService.delete(userToDelete.getId());

        // Verify that the delete method was called with the correct ID
        verify(userService, times(1)).delete(eq(userToDelete.getId()));
    }

    @Test
    @DisplayName("Return all users")
    void testFindAll() {
        User user = new User();
        List<User> listUser = new ArrayList<>();
        listUser.add(user);

        UserService userService = Mockito.mock(UserService.class);
        when(userService.findAll()).thenReturn(listUser);

        // Chame o método que você está testando que utiliza o userService

        // Verifique se o método foi chamado corretamente e se o resultado está correto
    }

    /**
     * 
     */
    @Test
    @DisplayName("Find user by id")
    void testFindById() {
        // Create a user with an ID
        User user = new User();
        user.setId(1L);

        // Mock the UserService
        UserService userService = Mockito.mock(UserService.class);

        // Mock the behavior of the findById method to return the User
        when(userService.findById(1L)).thenReturn(user);

        // Call the findById method with the ID
        User foundUser = userService.findById(1L);

        // Assert that the found user is not null
        assertNotNull(foundUser);

        // Assert that the ID of the found user matches the expected ID
        assertEquals(user.getId(), foundUser.getId());
    }

    @Test
    void testInsert() {
        // Create a new user object with sample data
        User user = new User();
        user.setId(1L);
        user.setName("John Doe");
        user.setEmail("johndoe@example.com");
        user.setPhone("1234567890");
        user.setPassword("password123");

        // Mock the UserService
        UserService userService = Mockito.mock(UserService.class);

        // Mock the behavior of the insert method to return the saved user
        when(userService.insert(any(User.class))).thenReturn(user);

        // Call the insert method with the user object
        User savedUser = userService.insert(user);

        // Assert that the saved user is not null
        assertNotNull(savedUser);

        // Assert that the saved user's ID is not the default value (0)
        assertNotEquals(0, savedUser.getId());

        // Assert that the saved user's data matches the input data
        assertEquals(user.getName(), savedUser.getName());
        assertEquals(user.getEmail(), savedUser.getEmail());
        assertEquals(user.getPhone(), savedUser.getPhone());
        assertEquals(user.getPassword(), savedUser.getPassword());
    }

    @Test
    void testUpdate() {
        // Create an existing user object with sample data
        User existingUser = new User();
        existingUser.setId(2L);
        existingUser.setName("Jane Smith");
        existingUser.setEmail("janesmith@example.com");
        existingUser.setPhone("9876543210");
        existingUser.setPassword("oldpassword");

        // Create an updated user object with new data
        User updatedUser = new User();
        updatedUser.setId(1L);
        updatedUser.setName("Jane Doe");
        updatedUser.setEmail("janedoe@example.com");
        updatedUser.setPhone("5555555555");
        updatedUser.setPassword("newpassword");

        // Mock the UserService
        UserService userService = Mockito.mock(UserService.class);

        // Mock the behavior of the update method to return the updated user
        when(userService.update(any(Long.class), any(User.class))).thenAnswer(invocation -> {
            Long id = invocation.getArgument(0);
            User obj = invocation.getArgument(1);

            if (id.equals(existingUser.getId())) {
                existingUser.setName(obj.getName());
                existingUser.setEmail(obj.getEmail());
                existingUser.setPhone(obj.getPhone());
                existingUser.setPassword(obj.getPassword());
                return existingUser;
            } else {
                throw new EntityNotFoundException();
            }
        });

        // Call the update method with the existing user ID and the updated user object
        User updatedUserResult = userService.update(existingUser.getId(), updatedUser);

        // Assert that the updated user is not null
        assertNotNull(updatedUserResult);

        // Assert that the updated user's data matches the updated data
        assertEquals(updatedUser.getName(), updatedUserResult.getName());
        assertEquals(updatedUser.getEmail(), updatedUserResult.getEmail());
        assertEquals(updatedUser.getPhone(), updatedUserResult.getPhone());
        assertEquals(updatedUser.getPassword(), updatedUserResult.getPassword());
    }

}
