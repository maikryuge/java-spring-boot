package com.ryuge.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ryuge.demo.entities.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{
	
	
}
