# java-spring-boot - DEMO

Este projeto é uma API REST que oferece endpoints para manipulação de usuários, produtos, pedidos e categorias.

## Endpoints

### Usuários

- **GET /users/{id}**: Retorna um usuário específico com base no ID fornecido.
- **PUT /users/{id}**: Atualiza um usuário existente com base no ID fornecido.
- **DELETE /users/{id}**: Exclui um usuário existente com base no ID fornecido.
- **GET /users**: Retorna uma lista de todos os usuários.
- **POST /users**: Cria um novo usuário.

### Produtos

- **GET /products/{id}**: Retorna um produto específico com base no ID fornecido.
- **GET /products**: Retorna uma lista de todos os produtos.

### Pedidos

- **GET /orders/{id}**: Retorna um pedido específico com base no ID fornecido.
- **GET /orders**: Retorna uma lista de todos os pedidos.

### Categorias

- **GET /categories/{id}**: Retorna uma categoria específica com base no ID fornecido.
- **GET /categories**: Retorna uma lista de todas as categorias.

## Instruções de Uso

1. Clone este repositório.
2. Instale as dependências necessárias.
3. Inicie o servidor.
4. Você pode acessar a documentação da API em http://localhost:8080/swagger-ui/index.html.

## Pré-requisitos

- Java JDK 8 ou superior
- Maven
- Docker (opcional)

## Configuração

1. Clone o repositório:

```
git clone https://github.com/seu-usuario/nome-do-projeto.git
```

2. Navegue até o diretório do projeto:

```
cd nome-do-projeto
```

3. Compile o projeto:

```
mvn clean install
```

4. Execute o aplicativo:

```
java -jar target/nome-do-projeto.jar
```

## Contribuindo

Contribuições são bem-vindas! Sinta-se à vontade para abrir um problema ou enviar um pull request.

## Licença

Este projeto está licenciado sob a [MIT License](https://opensource.org/licenses/MIT).

